# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2014, 2017, 2018, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2023-02-28 23:51+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "포함하는 폴더 열기"

#: baloosearchrunner.cpp:82
#, kde-format
msgid "Audios"
msgstr "오디오"

#: baloosearchrunner.cpp:83
#, kde-format
msgid "Images"
msgstr "그림"

#: baloosearchrunner.cpp:84
#, kde-format
msgid "Videos"
msgstr "비디오"

#: baloosearchrunner.cpp:85
#, kde-format
msgid "Spreadsheets"
msgstr "스프레드시트"

#: baloosearchrunner.cpp:86
#, kde-format
msgid "Presentations"
msgstr "프레젠테이션"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Folders"
msgstr "폴더"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Documents"
msgstr "문서"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Archives"
msgstr "압축 파일"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Texts"
msgstr "텍스트"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Files"
msgstr "파일"

#~ msgid "Search through files, emails and contacts"
#~ msgstr "파일, 이메일, 연락처 검색"

#~ msgid "Email"
#~ msgstr "이메일"

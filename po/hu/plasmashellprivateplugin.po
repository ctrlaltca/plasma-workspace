# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2014, 2019, 2020, 2021.
# SPDX-FileCopyrightText: 2022, 2023 Kristof Kiszel <ulysses@fsf.hu>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-29 01:38+0000\n"
"PO-Revision-Date: 2023-12-25 21:41+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ulysses@fsf.hu"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Ünnepek"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Események"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Tennivaló"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Egyéb"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 esemény"
msgstr[1] "%1 esemény"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Nincsenek események"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%2 %1"

#: calendar/qml/MonthViewHeader.qml:113
#, kde-format
msgid "Days"
msgstr "Nap"

#: calendar/qml/MonthViewHeader.qml:119
#, kde-format
msgid "Months"
msgstr "Hónap"

#: calendar/qml/MonthViewHeader.qml:125
#, kde-format
msgid "Years"
msgstr "Év"

#: calendar/qml/MonthViewHeader.qml:163
#, kde-format
msgid "Previous Month"
msgstr "Előző hónap"

#: calendar/qml/MonthViewHeader.qml:165
#, kde-format
msgid "Previous Year"
msgstr "Előző év"

#: calendar/qml/MonthViewHeader.qml:167
#, kde-format
msgid "Previous Decade"
msgstr "Előző évtized"

#: calendar/qml/MonthViewHeader.qml:184
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Ma"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgid "Reset calendar to today"
msgstr "Visszaállítás a mai napra"

#: calendar/qml/MonthViewHeader.qml:196
#, kde-format
msgid "Next Month"
msgstr "Következő hónap"

#: calendar/qml/MonthViewHeader.qml:198
#, kde-format
msgid "Next Year"
msgstr "Következő év"

#: calendar/qml/MonthViewHeader.qml:200
#, kde-format
msgid "Next Decade"
msgstr "Következő évtized"

#: calendar/qml/MonthViewHeader.qml:256
#, kde-format
msgid "Keep Open"
msgstr "Tartsa nyitva"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Beállítások…"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Képernyőzár bekapcsolva"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Beállítja, hogy a képernyő lezárjon-e a megadott idő letelte után."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "A képernyővédő időkorlátja"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Beállítja az időt, aminek a letelte után a képernyő lezár."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Új munkamenet"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Szűrők"

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:148
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""
"Ez az elem ismeretlen Plasma verzióhoz készült, nem kompatibilis a Plasma %1 "
"verzióval. Lépjen kapcsolatba az elem szerzőjével egy frissített verzióért."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:152
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""
"Ez az elem a Plasma %1 verziójához készült, és nem kompatibilis a Plasma %2 "
"verzióval. Lépjen kapcsolatba az elem szerzőjével egy frissített verzióért."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:156
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""
"Ez az elem a Plasma %1 verziójához készült, és nem kompatibilis a Plasma %2 "
"verzióval. Frissítse a Plasmát az elem használatához."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:161
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with the latest "
"version of Plasma. Please update Plasma in order to use the widget."
msgstr ""
"Ez az elem a Plasma %1 verziójához készült, és nem kompatibilis a Plasma  "
"legutóbbi verziójéval. Frissítse a Plasmát az elem használatához."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:111
msgctxt "applet category"
msgid "Accessibility"
msgstr "Akadálymentesítés"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Application Launchers"
msgstr "Alkalmazásindítók"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Astronomy"
msgstr "Csillagászat"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Date and Time"
msgstr "Dátum és idő"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Development Tools"
msgstr "Fejlesztőeszközök"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Education"
msgstr "Oktatás"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Environment and Weather"
msgstr "Környezet és időjárás"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Examples"
msgstr "Példák"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "File System"
msgstr "Fájlrendszer"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Szórakozás és játékok"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Graphics"
msgstr "Grafika"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Language"
msgstr "Nyelv"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Mapping"
msgstr "Térkép"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Miscellaneous"
msgstr "Egyéb"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Multimedia"
msgstr "Multimédia"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Online Services"
msgstr "Online szolgáltatások"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Productivity"
msgstr "Produktivitás"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "System Information"
msgstr "Rendszerjellemzők"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "Utilities"
msgstr "Segédprogramok"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr "Ablakok és feladatok"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Clipboard"
msgstr "Vágólap"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Tasks"
msgstr "Feladatok"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:148
#, kde-format
msgid "All Widgets"
msgstr "Minden widget"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:152
#, kde-format
msgid "Running"
msgstr "Fut"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:158
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Nem telepíthető"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:162
#, kde-format
msgid "Categories:"
msgstr "Kategóriák:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:232
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Új Plasma widgetek letöltése"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:241
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Telepítés helyi fájlból…"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:503
#, kde-format
msgid "Select Plasmoid File"
msgstr "Plasmoid fájl kiválasztása"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installing the package %1 failed."
msgstr "A(z) %1 csomag telepítése meghiúsult."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installation Failure"
msgstr "Telepítési hiba"

#~ msgid "&Execute"
#~ msgstr "Végr&ehajtás"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Sablonok"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Asztali szkriptkonzol"

#~ msgid "Editor"
#~ msgstr "Szerkesztő"

#~ msgid "Load"
#~ msgstr "Betöltés"

#~ msgid "Use"
#~ msgstr "Használat"

#~ msgid "Output"
#~ msgstr "Kimenet"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "Nem sikerült betölteni a szkriptfájlt: <b>%1</b>"

#~ msgid "Open Script File"
#~ msgstr "Szkriptfájl megnyitása"

#~ msgid "Save Script File"
#~ msgstr "Szkriptfájl mentése"

#~ msgid "Executing script at %1"
#~ msgstr "Szkript végrehajtása itt: %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Futásidő: %1 ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Háttérkép bővítmények letöltése"

#~ msgid "Containments"
#~ msgstr "Tartalmazó objektumok"

#~ msgctxt ""
#~ "%1 is a type of widgets, as defined by e.g. some plasma-packagestructure-"
#~ "*.desktop files"
#~ msgid "Download New %1"
#~ msgstr "Új %1 letöltése"

# translation of plasma_applet_battery.po to Frysk
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Rinse de Vries <rinsedevries@kde.nl>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-06 01:39+0000\n"
"PO-Revision-Date: 2009-06-26 00:19+0200\n"
"Last-Translator: Rinse de Vries <rinsedevries@kde.nl>\n"
"Language-Team: Frysk <kde-i18n-fry@kde.org>\n"
"Language: fy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/BatteryItem.qml:105
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr ""

#: package/contents/ui/BatteryItem.qml:166
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""

#: package/contents/ui/BatteryItem.qml:185
#, kde-format
msgid "Time To Full:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:186
#, kde-format
msgid "Remaining Time:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:191
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr ""

#: package/contents/ui/BatteryItem.qml:203
#, kde-format
msgid "Battery Health:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:209
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr ""

#: package/contents/ui/BatteryItem.qml:223
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr ""

#: package/contents/ui/CompactRepresentation.qml:105
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr ""

#: package/contents/ui/logic.js:23 package/contents/ui/logic.js:29
#: package/contents/ui/main.qml:142
#, kde-format
msgid "Fully Charged"
msgstr ""

#: package/contents/ui/logic.js:28
#, kde-format
msgid "Discharging"
msgstr ""

#: package/contents/ui/logic.js:30
#, kde-format
msgid "Charging"
msgstr ""

#: package/contents/ui/logic.js:32
#, kde-format
msgid "Not Charging"
msgstr ""

#: package/contents/ui/logic.js:35
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr ""

#: package/contents/ui/main.qml:117
#, fuzzy, kde-format
msgid "Power and Battery"
msgstr "Enerzjybehear"

#: package/contents/ui/main.qml:117 package/contents/ui/main.qml:286
#, fuzzy, kde-format
msgid "Power Management"
msgstr "Enerzjybehear"

#: package/contents/ui/main.qml:149
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr ""

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr ""

#: package/contents/ui/main.qml:153
#, kde-format
msgid "Battery at %1%, Charging"
msgstr ""

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Battery at %1%"
msgstr ""

#: package/contents/ui/main.qml:164
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr ""

#: package/contents/ui/main.qml:168
#, kde-format
msgid "No Batteries Available"
msgstr ""

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr ""

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr ""

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Not charging"
msgstr ""

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr ""

#: package/contents/ui/main.qml:188
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:192
#, kde-format
msgid "System is in Performance mode"
msgstr ""

#: package/contents/ui/main.qml:196
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:200
#, kde-format
msgid "System is in Power Save mode"
msgstr ""

#: package/contents/ui/main.qml:260
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr ""

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Failed to activate %1 mode"
msgstr ""

#: package/contents/ui/main.qml:311
#, kde-format
msgid "&Show Energy Information…"
msgstr ""

#: package/contents/ui/main.qml:317
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr ""

#: package/contents/ui/main.qml:329
#, kde-format
msgid "&Configure Energy Saving…"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:41
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:76
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:87
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/PowerManagementItem.qml:106
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:108
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:110
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:112
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:116
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:118
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:120
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:122
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:37
#, kde-format
msgid "Power Save"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:41
#, kde-format
msgid "Balanced"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:45
#, kde-format
msgid "Performance"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:62
#, kde-format
msgid "Power Profile"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:96
#, kde-format
msgctxt "Power profile"
msgid "Not available"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:190
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:192
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:194
#, fuzzy, kde-format
msgid "Performance mode is unavailable."
msgstr "Enerzjybehear"

#: package/contents/ui/PowerProfileItem.qml:207
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:209
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:211
#, fuzzy, kde-format
msgid "Performance may be reduced."
msgstr "Enerzjybehear"

#: package/contents/ui/PowerProfileItem.qml:222
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/PowerProfileItem.qml:240
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:259
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""

#, fuzzy
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "Enerzjybehear"

#, fuzzy
#~ msgid "General"
#~ msgstr "Algemien"

#, fuzzy
#~ msgid "Actions"
#~ msgstr "Aksjes"

#, fuzzy
#~ msgid "More..."
#~ msgstr "Mear..."

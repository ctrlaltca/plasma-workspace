# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrej Mernik <andrejm@ubuntu.si>, 2018.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-29 01:38+0000\n"
"PO-Revision-Date: 2023-11-29 22:33+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Poedit 3.4.1\n"

#: ui/DayNightView.qml:110
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"Začetek spreminjanja nočnih barv ob %1 in so popolnoma spremenjene ob %2"

#: ui/DayNightView.qml:113
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"Začetek spreminjanja nočnih barv ob %1 in so popolnoma spremenjene ob %2"

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Tapnite, da izberete lokacijo na zemljevidu."

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Kliknite, da izberete lokacijo na zemljevidu."

#: ui/LocationsFixedView.qml:78 ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr "Približaj"

#: ui/LocationsFixedView.qml:222
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Spremenjeno iz <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:235
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Zem. širina:"

#: ui/LocationsFixedView.qml:261
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Zem. dolžina:"

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr ""
"Modri filter spremeni barve zaslona v toplejše, da zmanjša obremenitve oči."

#: ui/main.qml:153
#, kde-format
msgid "Switching times:"
msgstr "Časi preklopov:"

#: ui/main.qml:156
#, kde-format
msgid "Always off"
msgstr "Vedno izključeno"

#: ui/main.qml:157
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Sončni zahod in vzhod na vaši lokaciji"

#: ui/main.qml:158
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Sončni zahod in vzhod na ročno določeni lokaciji"

#: ui/main.qml:159
#, kde-format
msgid "Custom times"
msgstr "Časi po meri"

#: ui/main.qml:160
#, kde-format
msgid "Always on night light"
msgstr "Vedno na nočni barvi"

#: ui/main.qml:183
#, kde-format
msgid "Day light temperature:"
msgstr "Dnevna temperatura barve:"

#: ui/main.qml:226 ui/main.qml:285
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1K"

#: ui/main.qml:230 ui/main.qml:289
#, kde-format
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "Hladno (brez filtra)"

#: ui/main.qml:236 ui/main.qml:295
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Toplo"

#: ui/main.qml:242
#, kde-format
msgid "Night light temperature:"
msgstr "Nočna temperatura barve:"

#: ui/main.qml:304
#, kde-format
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr "Trenutna lokacija:"

#: ui/main.qml:310
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Širina: %1°   Dolžina: %2°"

#: ui/main.qml:331
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"Lokacija naprave bo periodično osvežena z GPS (če je na voljo), ali s "
"pošiljanjem informacij o omrežju na  <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."

#: ui/main.qml:349
#, kde-format
msgid "Begin night light at:"
msgstr "Začni nočno barvo ob:"

#: ui/main.qml:362 ui/main.qml:385
#, kde-format
msgid "Input format: HH:MM"
msgstr "Vhodni format: HH:MM"

#: ui/main.qml:372
#, kde-format
msgid "Begin day light at:"
msgstr "Začni dnevno barvo ob:"

#: ui/main.qml:394
#, kde-format
msgid "Transition duration:"
msgstr "Trajanje prehoda:"

#: ui/main.qml:403
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuta"
msgstr[1] "%1 minuti"
msgstr[2] "%1 minute"
msgstr[3] "%1 minut"

#: ui/main.qml:416
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Vhod v minutah - najm. 1, najv. 600"

#: ui/main.qml:434
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Napaka: čas prehoda se prekriva."

#: ui/main.qml:456
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Lociram…"

#~ msgctxt "Night colour blue-ish"
#~ msgid "Cool"
#~ msgstr "Hladno"

#~ msgid "This is what day color temperature will look like when active."
#~ msgstr "Tako bo videti dnevna barvna temperature, kadar bo aktivna."

#~ msgid "This is what night color temperature will look like when active."
#~ msgstr "Tako bo videti nočna barvna temperatura, kadar bo aktivna."

#~ msgid "Activate blue light filter"
#~ msgstr "Omogoči filter modre barve"

#~ msgid "Turn on at:"
#~ msgstr "Vključi ob:"

#~ msgid "Turn off at:"
#~ msgstr "Ugasni ob:"

#~ msgid "Night Color begins changing back at %1 and ends at %2"
#~ msgstr "Začetek povratnih sprememb nočnih barv ob %1 in se končajo ob %2"

#~ msgid "Error: Morning is before evening."
#~ msgstr "Napaka: jutro je pred večerom."

#~ msgid ""
#~ "The device's location will be detected using GPS (if available), or by "
#~ "sending network information to <a href=\"https://location.services."
#~ "mozilla.com\">Mozilla Location Service</a>."
#~ msgstr ""
#~ "Lokacija naprave bo zaznana z GPS (če je na voljo), ali s pošiljanjem "
#~ "informacij o omrežju na <a href=\"https://location.services.mozilla.com"
#~ "\">Mozilla Location Service</a>."

#~ msgid "Night Color begins at %1"
#~ msgstr "Nočne barve se začnejo ob %1"

#~ msgid "Color fully changed at %1"
#~ msgstr "Barve popolnoma spremenjene ob %1"

#~ msgid "Normal coloration restored by %1"
#~ msgstr "Normalne barve obnovljene ob %1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Andrej Mernik,Matjaž Jeran"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "andrejm@ubuntu.si,matjaz.jeran@amis.net"

#~ msgid "Night Color"
#~ msgstr "Nočne barve"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid " K"
#~ msgstr " K"

#~ msgid "Operation mode:"
#~ msgstr "Način delovanja:"

#~ msgid "Automatic"
#~ msgstr "Samodejen"

#~ msgid "Times"
#~ msgstr "Čas"

#, fuzzy
#~| msgid "Sunrise begins"
#~ msgid "Sunrise begins:"
#~ msgstr "Sončni vzhod se začne"

#, fuzzy
#~| msgid "Sunset begins"
#~ msgid "Sunset begins:"
#~ msgstr "Sončni zahod se začne"

#, fuzzy
#~| msgid "and ends"
#~ msgid "...and ends:"
#~ msgstr "in se konča"

#~ msgid "(HH:MM)"
#~ msgstr "(HH:MM)"

# Malayalam translations for plasma-workspace package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2020-11-25 19:53+0000\n"
"Last-Translator: Vivek K J <vivekkj2004@gmail.com>\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 4.3.2\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "അടങ്ങിയിരിക്കുന്ന അറ തുറക്കുക"

#: baloosearchrunner.cpp:82
#, fuzzy, kde-format
#| msgid "Audio"
msgid "Audios"
msgstr "ശബ്ദം"

#: baloosearchrunner.cpp:83
#, fuzzy, kde-format
#| msgid "Image"
msgid "Images"
msgstr "ചിത്രം"

#: baloosearchrunner.cpp:84
#, fuzzy, kde-format
#| msgid "Video"
msgid "Videos"
msgstr "ചലച്ചിത്രം"

#: baloosearchrunner.cpp:85
#, fuzzy, kde-format
#| msgid "Spreadsheet"
msgid "Spreadsheets"
msgstr "സ്പ്രെഡ്ഷീറ്റ്"

#: baloosearchrunner.cpp:86
#, fuzzy, kde-format
#| msgid "Presentation"
msgid "Presentations"
msgstr "അവതരണം"

#: baloosearchrunner.cpp:87
#, fuzzy, kde-format
#| msgid "Folder"
msgid "Folders"
msgstr "അറ"

#: baloosearchrunner.cpp:88
#, fuzzy, kde-format
#| msgid "Document"
msgid "Documents"
msgstr "ഡോക്യുമെന്റ്"

#: baloosearchrunner.cpp:89
#, fuzzy, kde-format
#| msgid "Archive"
msgid "Archives"
msgstr "ശേഖരം"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Texts"
msgstr ""

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Files"
msgstr ""
